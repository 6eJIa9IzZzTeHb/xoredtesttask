/**
 * The main js file
 */
jQuery(function() {
	var myId = Math.floor(Math.random() * 9999999) + 1;
	var lastLoadedId = -1;
	var name = $("meta[name=nickname]").attr('value');
	var messages_container = $("#messages_container");
	
	/**
	 * Send message
	 */
	$('#send_message_form').submit(function(e) {
		
		var text =  $('#text_message').val();
		var but = $('#send_message_form button');
		if (text.length) {
			
			$('#text_message').val("");
			$.ajax({
				url: '/',
				method: 'GET',
				dataType: 'json',
				data: {
					action: 'send',
					message: text,
					lastLoadedId: lastLoadedId,
					name: name,
					myId: myId
				},
				success: function(result){
					console.log(result);
				},
			    error: function(a,b,c) {
			    	console.log(a,b,c);
			    }
			    
			});
		
		}
		
		
		return false;
		
	});
	
	/** 
	 * Refresh messages and draw if there is something new
	 */
	var refreshIsInProcess = false;
	var refreshMessages = function() {
		if (refreshIsInProcess) return;
		refreshIsInProcess = true;
		$.ajax({
			url: '/',
			method: 'GET',
			dataType: 'json',
			data: {
				action: 'get',
				lastLoadedId: lastLoadedId
			},
			success: function(result){
				//draw all messages
				$.each(result.messages, function(id, message) {
					drawNewMessage(message);
				});
				//refresh last index that we used to check
				lastLoadedId = result.lastIdLoaded;
				refreshIsInProcess = false;
			},
		    error: function(a,b,c) {
		    	console.log(a,b,c);
		    	refreshIsInProcess = false;
		    }
		    
		});
	}
	//refresh chat every second
	setInterval(refreshMessages, 1000);

	//here we going to show message, that we got from the server
	var isFirstMessage = true;
	var drawNewMessage = function(message) {
		if (isFirstMessage) {
			isFirstMessage = false;
			messages_container.html("");
		}
		
		//Rendering according to the type
		if (message.type == 1) {
			drawTextMessage(message);
		} else {
			drawPictureMessage(message);
		}
		
		messages_container.scrollTop(messages_container.height());
	}
	
	//Drawing text message from users
	var drawTextMessage = function(message) {
		var isMyMessage = message.myId == myId;
		var date = new Date(message.time);
		var hours = ('0'+date.getHours()).slice(-2);
		var minutes = ('0'+date.getMinutes()).slice(-2)
		var time = hours+':'+minutes;
		var html = "";
		var color = "#"+((1<<24)*Math.random()|0).toString(16);
		html += '<div class="col-md-12 ' +(isMyMessage?'text-left':'text-right')+ '">';
			html+= '<div class="message-body" style="background:'+color+'">';
				html += '<div class="message-container">';
					html += '<div class="message-status">'+message.name+' &nbsp&nbsp&nbsp <span class="glyphicon glyphicon-time message-time">'+time+'<span></div>';
					html += '<div class="message-text">'+message.text+'</div>';
				html += '</div>';
			html += '</div>';
		html += '</div>'
		$(html).appendTo(messages_container);
	}
	
	//Draws screenshot of the server desktop
	var drawPictureMessage = function (message) {
		var date = new Date(message.time);
		var hours = ('0'+date.getHours()).slice(-2);
		var minutes = ('0'+date.getMinutes()).slice(-2)
		var time = hours+':'+minutes;
		var html = "";
		var color = "#"+((1<<24)*Math.random()|0).toString(16);
		html += '<div class="col-md-12 text-center">';
			html+= '<div class="message-body" style="background:'+color+'">';
				html += '<div class="message-container">';
					html += '<div class="message-status">Server screenshot &nbsp&nbsp&nbsp <span class="glyphicon glyphicon-time message-time">'+time+'<span></div>';
					html += '<a href="'+message.src+'" data-lightbox="roadtrip"><img src="'+message.src+'" class="message-img"/></a>';
				html += '</div>';
			html += '</div>';
		html += '</div>'
		$(html).appendTo(messages_container);
		
		//We need reload it for all files, because this way we can add all pictures to one gallery
	    lightbox.option({
	        'resizeDuration': 200,
	        'wrapAround': true,
	        'disableScrolling': true
	      });
	}
});