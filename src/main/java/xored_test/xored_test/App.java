package xored_test.xored_test;


import backend.MainWindow;

/**
 * Hello world!
 *
 */
public class App 
{
	/** 
	 * Start point of the project
	 * @param args
	 */
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        
        MainWindow window = new MainWindow();
        window.open();
    }
    
}
