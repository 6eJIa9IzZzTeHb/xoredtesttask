package backend;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;

import server.HttpServer;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class MainWindow {
	
	/** Prefix labels texts*/
	public static String serverStatusText = "Status: ";
	public static String serverRequestsText = "Number of requests: ";
	
	
	/** Components */
	protected Shell shell;
	private Composite welcomeWindow;
	private Text portInput;
	private Composite statisticWindow;
	private Button btnScreenshot;
	private Label serverStatus;
	private Label serverRequests;
	private Button btnRestart;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				refreshStatistic();
				display.sleep();
			}
		}
		//close server on exit
		HttpServer.stopServer();
	}
	
	/**
	 * Action, when start server button is going to be used
	 */
	public void startServerButtonPressed() {
		//gets port from frame input
		int port = Integer.parseInt(portInput.getText());
		//starts server with this port
		HttpServer.startServer(port);
		//show statistic panel
		welcomeWindow.setVisible(false);
		statisticWindow.setVisible(true);
	}
	

	/**
	 * Action, when restart server button is going to be used
	 */
	public void restartServerButtonPressed() {
	 	//stop server
		HttpServer.stopServer();
		//show first panel
		welcomeWindow.setVisible(true);
		statisticWindow.setVisible(false);
	}
	
	
	/**
	 * While frame is alive checks server status, and renew label values
	 */
	public void refreshStatistic() {
		serverStatus.setText( serverStatusText + HttpServer.getServerStatus() );
		serverRequests.setText( serverRequestsText + HttpServer.getNumberOfRequests() );
	}
	
	/**
	 * Action, when screnshot button is going to be used
	 */
	public void screenshotButtonPressed() {
		//server will take a picture of the desktop, and our interface need to show the message about it
		HttpServer.sendScreenshot();
		
		MessageBox dialog =
			    new MessageBox(shell, SWT.OK);
		dialog.setText("Info");
		dialog.setMessage("Screenshoot sended");

		// open dialog and await user selection
		dialog.open();
	}

	/**
	 * Create contents of the window.
	 * Can be edit by WindowBuilder
	 */
	protected void createContents() {
		shell = new Shell(SWT.SHELL_TRIM & (~SWT.RESIZE));
		shell.setSize(535, 300);
		shell.setText("Xored test task chat");
				
						statisticWindow = new Composite(shell, SWT.NONE);
						statisticWindow.setBounds(10, 10, 509, 233);
						statisticWindow.setVisible(false);
						
						serverStatus = new Label(statisticWindow, SWT.NONE);
						serverStatus.setAlignment(SWT.CENTER);
						serverStatus.setLocation(0, 10);
						serverStatus.setSize(509, 20);
						serverStatus.setText("New Label");
						
						serverRequests = new Label(statisticWindow, SWT.NONE);
						serverRequests.setAlignment(SWT.CENTER);
						serverRequests.setText("New Label");
						serverRequests.setBounds(0, 56, 509, 20);
						
						btnScreenshot = new Button(statisticWindow, SWT.NONE);
						btnScreenshot.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent e) {
								screenshotButtonPressed();
							}
						});
						btnScreenshot.setText("Send screenshot");
						btnScreenshot.setBounds(166, 165, 184, 30);
						
						btnRestart = new Button(statisticWindow, SWT.NONE);
						btnRestart.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent e) {
								restartServerButtonPressed();
							}
						});
						btnRestart.setText("Restart");
						btnRestart.setBounds(166, 201, 184, 30);
				
				welcomeWindow = new Composite(shell, SWT.SHELL_TRIM & (~SWT.RESIZE));
				welcomeWindow.setLocation(0, 0);
				welcomeWindow.setSize(519, 244);
				
				Label lblPortNumber = new Label(welcomeWindow, SWT.NONE);
				lblPortNumber.setAlignment(SWT.CENTER);
				lblPortNumber.setBounds(168, 10, 184, 20);
				lblPortNumber.setText("Port number");
				
				portInput = new Text(welcomeWindow, SWT.BORDER);
				portInput.setText("8080");
				portInput.setBounds(148, 36, 243, 26);
				
				Button btnStartServer = new Button(welcomeWindow, SWT.NONE);
				btnStartServer.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						startServerButtonPressed();
					}
				});
				btnStartServer.setBounds(178, 177, 184, 30);
				btnStartServer.setText("Start the server");

	}
}
