package frontend;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import fi.iki.elonen.NanoHTTPD.Response;
import server.HttpServer;

/**
 * Searchs and send files with screenshots (form the some local folder)
 * @author Андрей
 *
 */
public class FileController {
	public static Response process(String fileName) throws IOException { 
        File file = new File(fileName);
		InputStream is = new FileInputStream(file);
		
		
        String mime = Files.probeContentType(file.toPath());
        if (mime == null) {
        	mime = "font/opentype";
        }
		return HttpServer.newChunkedResponse(Response.Status.OK,  mime, is);
		
	}
}
