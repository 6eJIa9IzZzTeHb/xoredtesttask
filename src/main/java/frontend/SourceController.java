package frontend;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import fi.iki.elonen.NanoHTTPD.Response;
import server.HttpServer;

/**
 * Searchs and returnds file from compiled resources
 * @author Андрей
 *
 */
public class SourceController {
	public static Response process(String fileName) throws IOException { 
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(fileName).getFile());
		InputStream is = classloader.getResourceAsStream(fileName);
		
		
        String mime = Files.probeContentType(file.toPath());
        if (mime == null) {
        	mime = "font/opentype";
        }
		return HttpServer.newChunkedResponse(Response.Status.OK,  mime, is);
		
	}
}
