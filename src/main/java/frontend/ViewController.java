package frontend;

import java.net.URLEncoder;
import java.util.Map;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import fi.iki.elonen.NanoHTTPD.Response;
import server.HttpServer;

/**
 * Render and returns views as a string
 * @author Андрей
 *
 */
public class ViewController {
	
	//css styles
	public static String[] styles = {
			"css/bootstrap.min.css",
			"css/site.css",
			"css/lightbox.css",
	};
	//js scripts
	public static String[] scripts = {
			"js/jquery-3.2.1.min.js",
			"js/bootstrap.min.js",
			"js/main.js",
			"js/lightbox.js"
	};
	
	/**
	 * Renders action via request
	 * @param action
	 * @param params
	 * @return
	 */
	public static Response process(String action, Map<String, String> params) {

        JtwigTemplate template = JtwigTemplate.classpathTemplate("templates/" + action + ".twig");
        JtwigModel model = JtwigModel.newModel();
        for (Map.Entry<String, String> pair: params.entrySet()) {
        	model.with(pair.getKey(), pair.getValue());
        }
	
        JtwigTemplate layoutTemplate = JtwigTemplate.classpathTemplate("templates/layout.twig");
        JtwigModel layoutModel = JtwigModel.newModel();        
        layoutModel.with("action", action);
        layoutModel.with("content", template.render(model));
        layoutModel.with("css", getCssListAsHtml());
        layoutModel.with("js", getJsListAsHtml());
       

       
        return HttpServer.newFixedLengthResponse( layoutTemplate.render(layoutModel) );	
	}
	
	/**
	 * Renders css styles as html
	 * @return
	 */
	public static String getCssListAsHtml() {
		return renderListAsHtml("css", styles);
	}
	
	/**
	 * Renders js scripts as html
	 * @return
	 */
	public static String getJsListAsHtml() {
		return renderListAsHtml("js", scripts);
	}
	
	/**
	 * Renders list as concatenate of rendered templates
	 * @param view
	 * @param list
	 * @return
	 */
	public static String renderListAsHtml(String view, String[] list) {
		String result = "";

		JtwigTemplate template = JtwigTemplate.classpathTemplate("templates/"+view+".twig");
		for (String file: list) {
	        @SuppressWarnings("deprecation")
			JtwigModel model = JtwigModel.newModel().with("file", URLEncoder.encode(file));
	        result += template.render(model);
		}
		
		return result;
	}
}
