package server;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.List;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fi.iki.elonen.NanoHTTPD;
import frontend.FileController;
import frontend.SourceController;
import frontend.ViewController;

/**
 * Wrapper over the nanoserver
 * @author Андрей
 *
 */
public class HttpServer extends NanoHTTPD {
	/** Status templates **/ 
	public static String STATUS_NOT_STARTED  = "The server is not started yet";
	public static String STATUS_ERROR  = "Error!!! ";
	public static String STATUS_RUNNING  = "The server is running! Ip: ";
	
	/**
	 * Message types
	 */
	public static int MESSAGE_TEXT = 1;
	public static int MESSAGE_PICTURE = 2;
	
	
	/**
	 * Server Data
	 */
    private static HttpServer server = null;
    public static int currentPort = 0;
    public static int reqNumber = 0;
    public static ArrayList<JSONObject> messages = new ArrayList<JSONObject>();
    public static ArrayList<String> screenshots = new ArrayList<String>();
    public static String status = STATUS_NOT_STARTED;
	
    /**
     * Server constructor
     * @param port
     * @throws IOException
     */
    private HttpServer(int port) throws IOException {
    	//start server
    	super(port);
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        
        //save port
        currentPort = port;
        System.out.println("\nRunning! Point your browsers to http://localhost:"+port+"/ \n");
    }
    
    /**
     * Returns the current server status
     * @return String
     */
    public static String getServerStatus() {
    	return status;
    }
    

    /**
     * Returns the number of requests, that has been sending to the server
     * @return String
     */
    public static int getNumberOfRequests() {
    	return reqNumber;
    }
    
    /**
     * Stops server if it`s running
     */
    public static void stopServer() {
    	if (server != null) {
    		server.stop();
    		server = null;

    		//clear variables
            currentPort = 0;
            reqNumber =  0;
            status = STATUS_NOT_STARTED;
            
            //removes all messages and files of saved screenshots
            messages.clear();
            for (String screen: screenshots) {
            	(new File(screen)).delete();
            }
            screenshots.clear();
            
        
    		System.out.println("Server stopped \n");

    	}
    }
    
    /**
     * Starts server with the port number
     * @param port
     */
    public static void startServer(int port) {
    	//if it was started yet
    	stopServer();
        try {
            server = new HttpServer(port);
            
    		String v4 = InetAddress.getLocalHost().toString();

            status = STATUS_RUNNING + v4 + ":" + currentPort;
        } catch (IOException ioe) {
            System.err.println("Couldn't start server:\n" + ioe);
            status = STATUS_ERROR + ioe;
        }
    }
    
    /**
     * handle to send message action
     * @return 
     */
    public static Response sendMessage(Map<String, String> params) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    	JSONObject message = new JSONObject();
    	message.put("time", timestamp.getTime());
    	message.put("text", params.get("message"));
    	message.put("myId", params.get("myId"));    	
    	message.put("name", params.get("name"));
    	message.put("type", MESSAGE_TEXT);
    	messages.add(message);
    	return HttpServer.newFixedLengthResponse("{\"status\":\"ok\"}");
    }

    /**
     * Saves screenshot and add it to messages
     */
    @SuppressWarnings("deprecation")
	public static void sendScreenshot() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		BufferedImage image;
		try {
			image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

	        String name = timestamp.getTime() + "-" + Math.floor(Math.random()*1000) + ".png";
			ImageIO.write(image, "png", new File(name));

	    	JSONObject message = new JSONObject();
	    	message.put("time", timestamp.getTime());
	    	message.put("src", "/?action=file&file="+URLEncoder.encode(name));
	    	message.put("type", MESSAGE_PICTURE);
	    	messages.add(message);
	    	
	    	screenshots.add(name);
	    	
		} catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    

    /**
     * handle to get message action
     * @return 
     */
    public static Response getMessages(Map<String, String> params) {
    	int lastIdLoaded = Integer.parseInt(params.get("lastLoadedId"));
    	
    	
    	Map result = new HashMap<String, String>();
    	JSONArray newMessages = new JSONArray();
    	if (lastIdLoaded != -1 && lastIdLoaded != messages.size()) {
    		while (lastIdLoaded < messages.size()) {
    			JSONObject obj = messages.get(lastIdLoaded);
    			newMessages.add(obj);
    			lastIdLoaded++;
    		}
    	}
    	lastIdLoaded = messages.size();

    	result.put("lastIdLoaded", lastIdLoaded);
    	result.put("messages", newMessages);
    	return HttpServer.newFixedLengthResponse((new JSONObject(result)).toJSONString());
    }
    
    
    /**
     * Handle to process requests to server
     */
    @Override
    public Response serve(IHTTPSession session) {
    	reqNumber++;
        Map<String, String> params = session.getParms();
      
        
        String action = params.get("action");
        if (action == null) {
        	action = "index";
        }
        if (action.equals("send")) {
        	return sendMessage(params);
        }
        if (action.equals("get")) {
        	return getMessages(params);
        }
        if (action.equals("file")) {
        	try {
				return FileController.process(params.get("file"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return HttpServer.newFixedLengthResponse(Response.Status.BAD_REQUEST, "text/plain", "Bad Request or wrong file path");
			}
        }
        if (action.equals("source")) {
        	try {
				return SourceController.process(params.get("file"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return HttpServer.newFixedLengthResponse(Response.Status.BAD_REQUEST, "text/plain", "Bad Request or wrong file path");
			}
        }
        return ViewController.process(action, params);
    }
}
