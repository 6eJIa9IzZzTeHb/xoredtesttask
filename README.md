**Sumary**
This application creates a local network server over optional port and give an oppotunity, to send text messages and screenshots from the server.

**Remarks**

1) You will not see messages sent before your login

2) Sometimes you need to use your ip, form ip config (ipv4 Ethernet), to connet from another device


**How to setup this app?**
You need to have a Eclipse 4.5(Mars+) with integrated Maven, WindowBuilder and Twig 


**Compile & Run**
To run this app you need to compile it via "maven package" and run from the target folder, or just compile and run it as a Java App (start point - "xored_test.xored_test.App")


**Tests**
The backend part was tested on Windows 8, the frontend - on Windows 8, 7 (in Opera, Firefox, Chrome) and on Android, on Chrome
![2017-06-29_16-35-28.png](https://bitbucket.org/repo/64zbx6E/images/1545435412-2017-06-29_16-35-28.png)
![2017-06-29_17-53-06.png](https://bitbucket.org/repo/64zbx6E/images/3111466669-2017-06-29_17-53-06.png)![2017-06-29_16-34-51.png](https://bitbucket.org/repo/64zbx6E/images/728352425-2017-06-29_16-34-51.png)



**Dependencies**

//Java dependencies

* SWT

* Nanohttpd

* Jtwig

* Json-simple


//Java plugins

* Window Builder

* Twig interpreter

* Maven


//JQury libraries

* Bootstrap

* JQuery

* Lightbox


**Instructuions**
Backend:

1) You need to start an app. There you can choose a port, that will be used for Server

2) That Server starts (If There is no errors), and you can open frontend part in your browser

3) You can make screenshots - all, what you need - is to press button on the server window

4) On restarting or closing server all screenshots will be removed from your computer


Frontend:

1) You need to pick your nickname, press ok

2) You can write to chat anything you want